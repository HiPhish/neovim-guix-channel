.. default-role: code

#############################
 Neovim channel for GNU Guix
#############################

This is an unofficial `GNU Guix`_ channel for `Neovim`_. The goal is to provide
an experimental testing ground where I can figure things out before sending a
patch to Guix itself.

If you encounter any problems or have any suggestions with this channel, please
file an issue so that it can be addressed before moving into Guix proper.


How to use
##########

This is a regular Guix channel, so follow the instructions regarding channels
as explained in the `official Manual`_ (`(guix)Channels`_ in `info`).


.. _GNU Guix: https://www.gnu.org/software/guix/
.. _Neovim: https://neovim.io/
.. _official Manual: http://guix.info/manual/en/Channels.html
.. _(guix)Channels: info:guix.info#Channels
