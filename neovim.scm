#!/usr/local/bin/guile -s
!#
;;; Copyright 2018 Alejandro Sanchez
;;;
;;; This file is part of neovim-guix.
;;; 
;;; Neovim-guix is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by the Free
;;; Software Foundation, either version 3 of the License, or (at your option)
;;; any later version.
;;; 
;;; Neovim-guix is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;; 
;;; You should have received a copy of the GNU General Public License
;;; along with neovim-guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (neovim)
  #:use-module ((guix packages)
                #:select (package origin base32))
  #:use-module ((guix utils)
                #:select (substitute-keyword-arguments version-major+minor))
  #:use-module ((guix git-download)
                #:select (git-fetch git-reference git-file-name))
  #:use-module ((guix licenses)
                #:select (asl2.0 vim) #:prefix license:)
  #:use-module ((guix build-system cmake)
                #:select (cmake-build-system))

  #:use-module ((gnu packages base) #:select (libiconv))
  #:use-module ((gnu packages gettext) #:select (gettext-minimal))
  #:use-module ((gnu packages gperf) #:select (gperf))
  #:use-module ((gnu packages jemalloc) #:select (jemalloc))
  #:use-module ((gnu packages libevent) #:select (libuv))
  #:use-module ((gnu packages lua)
                #:select (lua-5.1 luajit lua5.1-lpeg lua5.1-bitop))
  #:use-module ((gnu packages pkg-config) #:select (pkg-config))
  #:use-module ((gnu packages serialization)
                #:select (lua5.1-libmpack msgpack))
  #:use-module ((gnu packages terminals)
                #:select (libtermkey libvterm unibilium)))


(define-public neovim
  (package
    (name "neovim")
    (version "0.3.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
              (url "https://github.com/neovim/neovim")
              (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
          "15flii3p4g9f65xy9jpkb8liajrvhm5ck4j39z6d6b1nkxr6ghwb"))))

    (build-system cmake-build-system)
    (arguments
     `(#:modules ((srfi srfi-26)
                  (guix build cmake-build-system)
                  (guix build utils))
       #:phases
       (modify-phases %standard-phases
         ;; TODO: remove 'patch-tic on update
         ;; see: https://github.com/neovim/neovim/issues/9687
         (add-after 'unpack 'patch-tic
           (lambda _
             (substitute* "src/nvim/tui/tui.c"
               (("value != NULL") "value != NULL && value != (char *)-1"))
             #t))
         (add-after 'unpack 'set-lua-paths
           (lambda* (#:key inputs #:allow-other-keys)
             (let* ((lua-version "5.1")
                    (lua-cpath-spec
                     (lambda (prefix)
                       (let ((path (string-append prefix "/lib/lua/" lua-version)))
                         (string-append path "/?.so;" path "/?/?.so"))))
                    (lua-path-spec
                     (lambda (prefix)
                       (let ((path (string-append prefix "/share/lua/" lua-version)))
                         (string-append path "/?.lua;" path "/?/?.lua"))))
                    (lua-inputs (map (cute assoc-ref %build-inputs <>)
                                     '("lua"
                                       "lua-lpeg"
                                       "lua-bitop"
                                       "lua-libmpack"))))
               (setenv "LUA_PATH"
                       (string-join (map lua-path-spec lua-inputs) ";"))
               (setenv "LUA_CPATH"
                       (string-join (map lua-cpath-spec lua-inputs) ";"))
               #t))))))
    (inputs
     `(("lua" ,lua-5.1)
       ("libuv" ,libuv)
       ("msgpack" ,msgpack)
       ("libtermkey" ,libtermkey)
       ("libvterm" ,libvterm)
       ("unibilium" ,unibilium)
       ("jemalloc" ,jemalloc)
       ("libiconv" ,libiconv)
       ("luajit" ,luajit)
       ("lua" ,lua-5.1)
       ("lua-lpeg" ,lua5.1-lpeg)
       ("lua-bitop" ,lua5.1-bitop)
       ("lua-libmpack" ,lua5.1-libmpack)))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("gettext" ,gettext-minimal)
       ("gperf" ,gperf)))
    (home-page "http://neovim.io")
    (synopsis "Fork of vim focused on extensibility and agility")
    (description "Neovim is a project that seeks to aggressively
refactor Vim in order to:

@itemize
@item Simplify maintenance and encourage contributions
@item Split the work between multiple developers
@item Enable advanced external UIs without modifications to the core
@item Improve extensibility with a new plugin architecture
@end itemize\n")
    ;; Neovim is licensed under the terms of the Apache 2.0 license,
    ;; except for parts that were contributed under the Vim license.
    (license (list license:asl2.0 license:vim))))
